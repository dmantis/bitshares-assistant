FROM python:3.7.3

WORKDIR /app

RUN python3.7 -m pip install pipenv

COPY Pipfile Pipfile.lock ./

RUN pipenv install --clear --system --deploy

COPY ./src ./src

ENV PYTHONPATH ./

CMD ["python", "src/bot.py"]