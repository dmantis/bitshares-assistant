Telegram bot which notifies about new operations on account in bitshares blockchain.
 
Bot doesn't need access to private keys to operate. It only analyses open information from bitshares blockchain. In free version of [@bitshares_assistant_bot](https://t.me/bitshares_assistant_bot) user can add up to 3 accounts due to telegram send per second limitations. 

Of course, everyone can deploy their own bot instance and use it without any restrictions. 


## Deployment

Set corresponding environment variables in docker-compose.yml and run docker service.

You can copy following default docker-compose for convenience.
```yaml
version: "3.4"
services:
  bitshares-assistant:
    image: bitshares-assistant:0.2-beta
    environment:
      API_BOT_TOKEN: ""
      SENTRY_DSN: ""
      DB_DSN: "postgres://postgres:postgres@127.0.0.1:5432/postgres"
      BITSHARES_NODE: "wss://node.bitshares.eu"
    network_mode: host
```

Change database DSN to your settings, run migrations via flyway (/migrations directory, you may find convenient using flyway dockerhub image to do that).

After perfoming database migrations, run

```bash
docker-compose up -d
```

## LICENSE  

This software is distributed under terms of MIT License.  

Author: Dmitry Mantis. dmitry.mantis add_email_char_here protonmail com
