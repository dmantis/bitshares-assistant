CREATE TYPE e_lang AS ENUM (
  'EN',
  'RU',
  'DE',
  'CN'
);

CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  fullname VARCHAR(128),
  username VARCHAR(128),
  -- todo default date in the past
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  pro_dt TIMESTAMP WITH TIME ZONE,
  status BOOLEAN DEFAULT TRUE,
  lang e_lang DEFAULT 'EN'
);

-- todo index?

CREATE TABLE accounts (
  id INTEGER PRIMARY KEY,
  name VARCHAR(128) NOT NULL UNIQUE,
  last_op INTEGER DEFAULT 0
);

CREATE TABLE association (
  user_id INTEGER,
  account_id INTEGER,
  PRIMARY KEY (user_id, account_id)
);
