import logging
import functools
import asyncio

import sentry_sdk
from aiogram import Bot, Dispatcher, executor, types
from aiogram.contrib.middlewares.i18n import I18nMiddleware

from src.workers import NotifierWorker, TelegramSender
from src.models import User, Account, Session
from src.loader import get_loader
from src.exceptions import *
from src.settings import API_BOT_TOKEN, LOG_LEVEL, SENTRY_DSN, BITSHARES_NODE, \
    MAX_ACCOUNTS_PER_USER

# todo user lang enum in migrations and model

logging.basicConfig(level=LOG_LEVEL)
logger = logging.getLogger('bot')

if SENTRY_DSN:
    sentry_sdk.init(SENTRY_DSN)

session = Session()
bot = Bot(token=API_BOT_TOKEN)
dp = Dispatcher(bot)

# enable localizations
i18n = I18nMiddleware('bot', 'locales')
dp.middleware.setup(i18n)
_ = i18n.gettext

# bitshares data loader
loader = get_loader(node=BITSHARES_NODE)


def user_getter(func):
    @functools.wraps(func)
    async def wrapper(message: types.Message):
        user = session.query(User).filter_by(id=message.from_user.id).first()
        if not user:
            user = User(
                message.from_user.id,
                message.from_user.full_name,
                message.from_user.username
            )
            session.add(user)
            session.commit()
        return await func(message, user)
    return wrapper


async def get_account(account_name: str) -> Account:
    # todo move db load to loader
    account = session.query(Account).filter_by(name=account_name).first()
    if not account:
        account = await loader.get_account(account_name)
    return account


@dp.message_handler(commands=['start', 'help'])
async def welcome(message: types.Message):
    welcome_msg = _("""
Welcome to BitShares Assistant!

To start monitor new account simply add it with /add command.
/add _bitshares account name_
/stop to stop getting updates.
/resume to resume getting updates.
/help to print this message again.
/orders to view your current orders.
/status to print information about your settings.
/delete _bitshares account name_ to delete account from monitorin
/deleteall to delete all information about you from our database (all settings will be lost).
""")
    await message.reply(welcome_msg)


@dp.message_handler(commands=['add'])
@user_getter
async def add_account_cmd(message: types.Message, user: User):
    msg_data = message.text.split()

    # todo already added?
    try:
        account_name = msg_data[1]
        if len(user.accounts) < MAX_ACCOUNTS_PER_USER:
            account = await get_account(account_name)
            user.accounts.append(account)
        else:
            raise SubscriptionRequiredError()

        logger.info(f"user {user} adds {account_name} to monitoring")
        resp = _("Account *{account_name}* has been successfuly added").format(
            account_name=account_name
        )

    except IndexError:
        resp = "Usage: /add account_name"

    except SubscriptionRequiredError:
        resp = _('subscription_message').format(message.chat.id)
        session.rollback()

    # todo
    except InvalidAccountNameError:
        resp = _("Invalid account name")
        logging.warning(f"attempt to use invalid account name: '{account_name}'")
        session.rollback()

    # todo
    except AccountDoesNotExistsError:
        resp = _("Account *{account_name}* doesn't exist").format(
            account_name=account_name)
        logging.warning(resp)
        session.rollback()

    session.commit()
    await message.reply(resp, parse_mode='Markdown')


@dp.message_handler(commands=['delete'])
@user_getter
async def delete_cmd(message: types.Message, user: User):
    msg_data = message.text.split()
    if len(msg_data < 2):
        resp = _("Usage: /delete account_name")
    else:
        try:
            account_name = msg_data[1]
            user.delete_account(await get_account(account_name))
            logger.info(f"user {user} removed {account_name} from monitoring")
            resp = _("Account *{account_name}* has been removed").format(
                account_name=account_name
            )
        except AccountNotLinkedError:
            resp = _("Account *{account_name}* is not in your monitor "
                     "list").format(account_name=account_name)
            session.rollback()
        except (InvalidAccountNameError, AccountDoesNotExistsError):
            resp = _("Invalid account request")
            session.rollback()
    session.commit()
    await message.reply(resp, parse_mode='Markdown')


@dp.message_handler(commands=['deleteall'])
@user_getter
async def delete_all_cmd(message: types.Message, user: User):
    session.delete(user)
    session.commit()
    logger.info(f"user {user} has been deleted")
    resp = _("Your user information has been deleted")
    await message.reply(resp)


@dp.message_handler(commands=['resume'])
@user_getter
async def resume_cmd(message: types.Message, user: User):
    user.status = True
    session.commit()
    logger.info('user {} resumed notifications'.format(user))
    resp = _('Notifications has been resumed')
    await message.reply(resp)


@dp.message_handler(commands=['stop'])
@user_getter
async def stop_cmd(message: types.Message, user: User):
    user.status = False
    session.commit()
    logger.info(f"user {user} stopped notifications")
    resp = _("Notifications has been stopped")
    await message.reply(resp)


@dp.message_handler(commands=['status'])
@user_getter
async def status_cmd(message: types.Message, user: User):
    info_msg = _("Your current accounts and monitor status: {accounts_list}"
                 "Notifications are *{status}*")
    resp = info_msg.format(**user.get_info())
    await message.reply(resp)


# @dp.message_handler(commands=['orders'])
# @user_getter
# async def orders_cmd(message: types.Message, user: User):
#     #  logger.info('user {} list orders command'.format(user))
#     if len(user.accounts) != 1:
#         resp = "Orders can be viewed only if you monitor one account (will be changed soon)"
#     else:
#         resp = '\n'.join(map(str, BTSAccount(user.accounts[0].id).openorders))
#     await message.reply(resp)


if __name__ == '__main__':
    logger.info('service has been started')
    loop = asyncio.get_event_loop()
    telegram_sender_worker = TelegramSender(API_BOT_TOKEN)
    notifier_worker = loop.run_until_complete(
        NotifierWorker(
            node=BITSHARES_NODE,
            sender_queue=telegram_sender_worker.queue
        ).async_init()
    )
    telegram_sender_worker.on('blocked', notifier_worker.disable_user)
    loop.create_task(notifier_worker.run())
    loop.create_task(telegram_sender_worker.run())
    executor.start_polling(dp, skip_updates=True, loop=loop)
