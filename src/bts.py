import asyncio
import json
import logging

import aiohttp
from async_lru import alru_cache


logger = logging.getLogger()


class BitSharesException(Exception):
    pass


class BitSharesAsync(object):
    _jsonrpc_counter = 1

    def __init__(self,
                 node: str):
        self._node = node
        self._session = None

    async def async_init(self, loop: asyncio.AbstractEventLoop = None):
        if not loop:
            loop = asyncio.get_event_loop()
        self._session = aiohttp.ClientSession(loop=loop)
        return self

    async def close(self):
        await self._session.close()

    async def _post(self, api, method, *params):
        data = json.dumps({
            "jsonrpc": "2.0",
            "method": "call",
            "params": [api, method, params],
            "id": self._jsonrpc_counter
        })
        self._jsonrpc_counter += 1
        logger.debug('send: ' + data)
        async with self._session.post(self._node, data=data) as response:
            resp = await response.text()
        logger.debug('recv: ' + resp)
        resp = json.loads(resp)
        if 'error' in resp:
            raise BitSharesException(resp)
        return resp['result']

    async def get_account_history(
            self,
            account_id_or_name: str,
            stop: str = "1.11.0",
            limit: int = 100,
            start: str = "1.11.0"
    ):
        return await self._post("history", "get_account_history",
                                account_id_or_name, stop, limit, start)

    async def get_account_history_operations(
            self,
            account_id_or_name: str,
            op_type: int = 0,
            start: str = "1.11.0",
            stop: str = "1.11.0",
            limit: int = 100):
        return await self._post("history", "get_account_history_operations",
                                account_id_or_name, op_type, start, stop, limit)

    @alru_cache(maxsize=128)
    async def get_assets(self, *assets):
        return await self._post(0, "get_assets", assets)

    async def get_accounts(self, *account_ids_or_names):
        return await self._post(0, "get_accounts", account_ids_or_names)


if __name__ == '__main__':
    pass
