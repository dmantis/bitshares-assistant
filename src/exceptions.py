"""
    Exception used in services
"""

class RequiredFieldMissedError(Exception):
    pass


class RequiredOptionMissedError(Exception):
    pass


class InvalidTelegramIDError(Exception):
    pass


class EmptyFromFieldError(Exception):
    pass


class EmptyChatFieldError(Exception):
    pass


class InvalidAccountNameError(Exception):
    pass


class AccountDoesNotExistsError(Exception):
    pass


class SubscriptionRequiredError(Exception):
    pass


class UnknownOperationCodeError(Exception):
    pass


class AccountNotLinkedError(Exception):
    pass


class UnsupportedLanguage(Exception):
    pass
