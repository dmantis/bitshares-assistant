"""
accordance between op_codes and operations
https://github.com/abitmore/bitshares-core/blob/170523826b82ba754eeae8706a891797b4b37ee8/libraries/chain/include/graphene/chain/protocol/operations.hpp#L50
"""

import logging

from src.models import Amount, Asset
from src.loader import get_loader
from src.exceptions import RequiredFieldMissedError

from src.settings import BITSHARES_NODE

logger = logging.getLogger('notifier')



class Rate(object):
    def __init__(self, base: Amount, quote: Amount):
        self.base = base
        self.quote = quote

    def inverse(self):
        return Rate(self.quote, self.base)

    def amount(self):
        # todo what to do if self.quote.amount == 0? where to handle better
        return self.base.amount / self.quote.amount

    def __str__(self):
        # todo how to show right? inverse by logic.
        return f"{self.amount} " + "{self.base.symbol}/{self.quote.symbol}"


class Operation(object):
    __operation__ = None
    __id__ = None
    __schema__ = []
    _op_dict = {}

    def __init__(self, op_info: dict, loader=None):
        self._op = op_info['op'][1]
        self._result = op_info['result'][1]
        for field in self.__schema__:
            if field not in self._op:
                raise RequiredFieldMissedError(
                    f"Missed required field '{field}' in {self.__operation__}"
                )
        if not loader:
            self._loader = get_loader(node=BITSHARES_NODE)

    def __init_subclass__(cls, **kwargs):
        # construct pythonic operation name from class name.
        cls.__operation__ = cls.get_operation_name_from_cls(cls)
        # register the operation in global op registry
        Operation._op_dict[cls.__id__] = cls

    @staticmethod
    def get_operation_name_from_cls(cls):
        result = ""
        first_letter = True
        for c in cls.__name__.replace('Operation', ''):
            if first_letter:
                result += c.lower()
                first_letter = False
            elif c.isupper():
                result += '_' + c.lower()
            else:
                result += c
        return result

    async def load(self):
        return None

    @classmethod
    def get_op_cls_by_id(cls, operation_id: int):
        return cls._op_dict.get(operation_id, None)


class TransferOperation(Operation):
    __slots__ = ('amount', 'account_to', 'account_from')
    __id__ = 0
    __schema__ = ['from', 'to', 'amount']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.account_from, self.account_to, self.amount = (None,) * 3

    async def load(self):
        self.account_from = await self._loader.get_account(self._op['from'])
        self.account_to = await self._loader.get_account(self._op['to'])
        self.amount = await self._loader.get_amount(**self._op['amount'])

    def __str__(self):
        return f"transfer {self.amount} from *{self.account_from.name}* " \
            f"to *{self.account_to.name}*"


class LimitOrderCreateOperation(Operation):
    __slots__ = ('amount1', 'amount2', 'rate')
    __id__ = 1
    __schema__ = ['amount_to_sell', 'min_to_receive']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.amount1, self.amount2, self.rate = None, None, None

    async def load(self):
        self.amount1 = await self._loader.get_amount(
            **self._op['amount_to_sell'])
        self.amount2 = await self._loader.get_amount(
            **self._op['min_to_receive'])
        self.rate = Rate(self.amount1, self.amount2)

    def __str__(self):
        return f"Wants to sell *{self.amount1}* for *{self.amount2}*\n" \
            f"{self.rate}"


class LimitOrderCancelOperation(Operation):
    __id__ = 2

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.amount = None

    async def load(self):
        self.amount = await self._loader.get_amount(**self._result['amount'])

    def __str__(self):
        return f"cancel order on {self.amount}"


# class CallOrderUpdateOperation(Operation):
#     __operation__ = 'call_order_update'
#     __id__ = 3
#
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#
#     async def load(self):
#         pass
#
#     def __str__(self):
#         return ""


class FillOrderOperation(Operation):
    __id__ = 4
    __schema__ = ['receives', 'pays']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.amount1, self.amount2, self.rate = None, None, None

    async def load(self):
        self.amount1 = await self._loader.get_amount(**self._result['receives'])
        self.amount2 = await self._loader.get_amount(**self._result['pays'])

    def __str__(self):
        return f"bought *{self.amount1}* for *{self.amount2}*\n{self.rate}"


class AccountCreateOperation(Operation):
    __id__ = 5
    __schema__ = ['name', 'referrer_percent', 'registrar', 'referrer']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.registrar = None
        self.referrer = None

    async def load(self):
        self.registrar = await self._loader.get_account(self._op['registrar'])
        self.referrer = await self._loader.get_account(self._op['referrer'])

    def __str__(self):
        new_account = self._op['name']
        return f"*{self.registrar.name}* register new account " \
            f"*{new_account}* thanks to *{self.referrer.name}*"


class AccountUpdateOperation(Operation):
    __id__ = 6
    __schema__ = []

    def __str__(self):
        return "update account"


class AccountWhitelistOperation(Operation):
    __id__ = 7
    __schema__ = ['authorizing_account', 'account_to_list']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.auth_account = None
        self.account_to_list = None

    async def load(self):
        self.auth_account = await self._loader.get_account(
            self._op['authorizing_account'])
        self.account_to_list = await self._loader.get_account(
            self._op['account_to_list'])

    def __str__(self):
        return f"{self.auth_account.name} whitelisted {self.account_to_list.name}"


class AccountUpgradeOperation(Operation):
    __id__ = 8
    __schema__ = []

    def __str__(self):
        return "upgrade account"


class AccountTransferOperation(Operation):
    __id__ = 9
    __schema__ = []

    def __str__(self):
        return "transfer account ownership"


class AssetCreateOperation(Operation):
    __id__ = 10
    __schema__ = ['symbol', 'precision']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.asset = None

    async def load(self):
        self.asset = Asset(
            self._result, self._op['symbol'], self._op['precision'])

    def __str__(self):
        return f"create asset {self.asset} with precision " \
            f"{self._op['precision']}"


class AssetUpdateOperation(Operation):
    __id__ = 11
    __schema__ = ['asset_to_update']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.asset = None

    async def load(self):
        self.asset = await self._loader.get_asset(self._op['asset_to_update'])

    def __str__(self):
        return f"update asset {self.asset}"


class AssetUpdateBitassetOperation(Operation):
    __id__ = 12
    __schema__ = ['asset_to_update']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.asset = None

    async def load(self):
        self.asset = await self._loader.get_asset(self._op['asset_to_update'])

    def __str__(self):
        # todo new options
        return f"update bitasset {self.asset}"


class AssetIssueOperation(Operation):
    __id__ = 14
    __schema__ = ['asset_to_issue', 'issue_to_account', 'issuer']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.amount = None
        self.account_from = None
        self.account_to = None

    async def load(self):
        self.amount = await self._loader.get_amount(**self._op['asset_to_issue'])
        self.account_from = await self._loader.get_account(self._op['issuer'])
        self.account_to = await self._loader.get_account(
            self._op['issue_to_account'])

    def __str__(self):
        return f"{self.account_from} issued {self.amount} to {self.account_to}"


class AssetReserveOperation(Operation):
    __id__ = 15
    __schema__ = ['amount_to_reserve', 'payer']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.amount = None

    async def load(self):
        self.amount = await self._loader.get_amount(
            **self._op['amount_to_reserve'])

    def __str__(self):
        return f"burn {self.amount}"


class AssetFundFeePoolOperation(Operation):
    __id__ = 16
    __schema__ = ['from_account', 'asset_id', 'amount']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.asset, self.amount = None, None

    async def load(self):
        self.asset = await self._loader.get_asset(self._op['asset_id'])
        self.amount = await self._loader.get_amount(**self._op['amount'])

    def __str__(self):
        return f"funds {self.asset} fee pool with {self.amount}"


class SettleOperation(Operation):
    __id__ = 17
    __schema__ = ['amount']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.amount = None

    async def load(self):
        self.amount = await self._loader.get_amount(**self._op['amount'])

    def __str__(self):
        return f"settle {self.amount}"


class AssetPublishFeedOperation(Operation):
    __id__ = 19
    __schema__ = ['asset_id']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.asset = None

    async def load(self):
        self.asset = await self._loader.get_asset(self._op['asset_id'])

    def __str__(self):
        # todo feed field
        return f"publish {self.asset} feed"


class VestingBalanceCreateOperation(Operation):
    __id__ = 32
    __schema__ = ['owner', 'amount']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.amount = None
        self.owner = None

    async def load(self):
        self.amount = await self._loader.get_amount(**self._op['amount'])
        self.owner = await self._loader.get_account(self._op['owner'])

    def __str__(self):
        # todo vesting balances types
        return f"create vesting balance of {self.amount} which can be " \
            f"withdrawn by {self.owner}"


class VestingBalanceWithdrawOperation(Operation):
    __id__ = 33
    __schema__ = ['amount']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.amount = None

    async def load(self):
        self.amount = await self._loader.get_amount(**self._op['amount'])

    def __str__(self):
        return f"withdraw {self.amount} from vesting balance"


class WorkerCreateOperation(Operation):
    __id__ = 34
    __schema__ = ['work_begin_date', 'work_end_date', 'daily_pay', 'name', 'url']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __str__(self):
        # todo sanitize worker name (markdown)
        # todo daily pay
        # worker_name = self._op['name'].replace()
        return f"create worker *{self._op['name']}*, url: {self._op['url']}, " \
            f"{self._op['work_begin_date']} - {self._op['work_end_date']}"


class BlindTransferOperation(Operation):
    __id__ = 40
    __schema__ = []

    def __str__(self):
        return "make blind transfer"


class BidCollateralOperation(Operation):
    __id__ = 45
    __schema__ = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    async def load(self):
        pass

    def __str__(self):
        return "bid collateral"
