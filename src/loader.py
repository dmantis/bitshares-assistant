import asyncio
from async_lru import alru_cache

from src.bts import BitSharesAsync
from src.models import Account, Asset, Amount


class Loader(object):
    __instances = {}

    def __init__(self, node: str, db=None, loop=None):
        self._bts = BitSharesAsync(node=node)
        self._db = db
        self._initiated = False
        if loop is None:
            loop = asyncio.get_event_loop()
        self._loop = loop

    async def _async_init(self):
        # todo lock?
        if not self._initiated:
            await self._bts.async_init(loop=self._loop)
            self._initiated = True

    @classmethod
    def create_loader(cls, node: str, loop: asyncio.AbstractEventLoop = None):
        if cls.__instances.get(node):
            raise Exception("loader already created")
        return Loader(node=node, loop=loop)

    @classmethod
    def get_loader(
            cls,
            node: str,
            create_if_not_exist: bool = True,
            loop: asyncio.AbstractEventLoop = None
    ):
        if not cls.__instances.get(node) and create_if_not_exist:
            cls.__instances[node] = cls.create_loader(node, loop=loop)
        return cls.__instances[node]

    # todo add db load
    @alru_cache(maxsize=256)
    async def get_account(self, account_id_or_name: (str, int)) -> Account:
        await self._async_init()
        if isinstance(account_id_or_name, int):
            account_id_or_name = f"1.2.{account_id_or_name}"
        accounts_list = await self._bts.get_accounts(account_id_or_name)
        return Account(accounts_list[0]['id'], accounts_list[0]['name'])

    # todo add db load
    async def get_asset(self, asset_id: (str, int)) -> Asset:
        await self._async_init()
        assets_list = await self._bts.get_assets(asset_id)
        return Asset(id_=assets_list[0]['id'], **assets_list[0])

    async def get_amount(self, asset_id: (str, int), amount: int) -> Amount:
        asset = await self.get_asset(asset_id)
        return Amount(asset, amount)


def create_loader(node: str, loop: asyncio.AbstractEventLoop = None):
    return Loader.create_loader(node=node, loop=loop)


def get_loader(node: str):
    return Loader.get_loader(node)


# debug purposes
if __name__ == '__main__':
    pass
