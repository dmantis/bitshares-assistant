import string
import enum

from sqlalchemy import Integer, String, Boolean, DateTime, Column, Table, Enum
from sqlalchemy import create_engine, PrimaryKeyConstraint, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker

from src.exceptions import *
from src.settings import DB_DSN

from datetime import datetime

# todo move to gino

engine = create_engine(DB_DSN)
Session = sessionmaker(bind=engine)
Base = declarative_base()

association_table = Table('association', Base.metadata,
    Column('user_id', Integer, ForeignKey('users.id'), primary_key=True),
    Column('account_id', Integer, ForeignKey('accounts.id'), primary_key=True),
    PrimaryKeyConstraint('user_id', 'account_id')
)

# logging.getLogger('sqlalchemy.engine').setLevel(logging.DEBUG)

class LANGUAGES(enum.Enum):
    EN = 0
    RU = 1
    DE = 2
    CN = 3


def get_object_id(id_: str) -> int:
    if isinstance(id_, str) and id_.count('.') == 2:
        id_ = int(id_.rsplit('.', 1)[1])
    return id_


class User(Base):
    """ Telegram User. Many-To-Many relationship with Account. """
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    fullname = Column(String(128), default='')
    username = Column(String, default='')
    created_at = Column(DateTime)
    pro_dt = Column(DateTime, default=datetime(1990, 1, 1, 0, 0, 0, 0))
    status = Column(Boolean, default=True)
    lang = Column(Enum(LANGUAGES), default=LANGUAGES.EN)
    accounts = relationship("Account", secondary=association_table,
                            back_populates="users")

    def __init__(self, id_, fullname='', username='', lang=LANGUAGES.EN):
        self.id = id_
        self.fullname = fullname
        self.username = username
        self.lang = lang

    def set_lang(self, lang: str):
        if not lang in LANGUAGES:
            raise UnsupportedLanguage(lang)
        else:
            self.lang = LANGUAGES[lang]

    def __repr__(self):
        pro = 'pro' if self.pro else 'regular'
        return f"<User({self.id},'{self.fullname}',{pro} account)>"

    def __str__(self):
        return f"<User {self.id} - '{self.username}'>"

    # todo bot cmd for admin account and logic for incrementing
    @property
    def pro(self):
        return self.pro_dt > datetime.utcnow()

    def get_info(self):
        accounts_list = ('\n • {}'.format(a.name) for a in self.accounts)
        info_dict = {
            'accounts_list': ''.join(accounts_list),
            'status': 'on' if self.status else 'off'
        }
        return info_dict

    def delete_account(self, account):
        try:
            self.accounts.remove(account)
        except ValueError:
            raise AccountNotLinkedError()


class Account(Base):
    """ BitShares Account. Many-To-Many relationship with User. """
    __tablename__ = 'accounts'

    id = Column(Integer, primary_key=True)
    # todo remove index from name and work with ids
    name = Column(String, unique=True, index=True)
    last_op = Column(Integer, default=0)
    users = relationship("User", secondary=association_table,
                         back_populates="accounts")

    def __init__(self, id_: (int, str), name: str):
        # todo move validator somewhere else?
        allowed_chars = string.ascii_lowercase + string.digits + '-.'
        if any(c not in allowed_chars for c in name):
            raise InvalidAccountNameError()
        self.id = get_object_id(id_)
        self.name = name

    def update_last_op(self, last_op):
        self.last_op = get_object_id(last_op)

    def __repr__(self):
        return f"<Account('{self.name}','{self.id}')>"


# todo cache in db or not?
class Asset(object):
    def __init__(self, id_: (str, int), symbol: str, precision: int, *args, **kwargs):
        self.id = get_object_id(id_)
        self.symbol = symbol
        self.precision = precision

    def __str__(self):
        return self.symbol


class Amount(object):
    def __init__(
            self,
            asset: Asset,
            raw_amount: int = 0,
    ):
        self.asset = asset
        self.raw_amount = int(raw_amount)

    @property
    def amount(self):
        return self.raw_amount / (10 ** self.asset.precision)

    # todo: REWRITE WITH SAFE DECIMAL HANDLE
    def __str__(self):
        number_s = format(self.amount, '.9f').rstrip('0')
        number_s = number_s + '0' if number_s[-1] == '.' else number_s
        return number_s + ' ' + self.asset.symbol
