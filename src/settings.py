from envparse import env

API_BOT_TOKEN = env('API_BOT_TOKEN')
DB_DSN = env('DB_DSN')
BITSHARES_NODE = env('BITSHARES_NODE', default="wss://node.bitshares.eu", cast=str)
CHECK_INTERVAL = env('CHECK_INTERVAL', default=15, cast=int)

SENTRY_DSN = env('SENTRY_DSN', default='', cast=str)
LOG_LEVEL = env('LOG_LEVEL', default='INFO', cast=str)

MAX_ACCOUNTS_PER_USER = env('MAX_ACCOUNTS_PER_USER', default=3, cast=int)


