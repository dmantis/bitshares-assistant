import logging
import asyncio

from typing import List

import sentry_sdk

from src.models import Account, Session, User, association_table
from src.bts import BitSharesAsync, BitSharesException
from src.settings import LOG_LEVEL, SENTRY_DSN, CHECK_INTERVAL, BITSHARES_NODE
from src.handlers import Operation

logger = logging.getLogger('notifier')
logger.setLevel(LOG_LEVEL)
if SENTRY_DSN:
    sentry_sdk.init(SENTRY_DSN)

session = Session()

# todo parse blocks, not accounts history - 1 call per time instead of 100s.


class NotifierWorker(object):
    def __init__(
            self,
            node: str = BITSHARES_NODE,
            check_interval: float = 15.0,
            sender_queue: asyncio.Queue = None
    ):
        self._bts = BitSharesAsync(node=node)
        self.check_interval = check_interval
        self._queue = sender_queue

    async def async_init(self):
        await self._bts.async_init()
        return self

    async def account_updates_iter(self, account: Account):
        account_history = await self._bts.get_account_history(
            f"1.2.{account.id}", stop=f"1.11.{account.last_op}")
        # if newly added account
        if account.last_op == 0:
            account.update_last_op(account_history[0]['id'])
            session.commit()
        else:
            for entry in account_history[::-1]:
                account.update_last_op(entry['id'])
                session.commit()
                yield await self.process_history_entry(entry)

    @staticmethod
    async def process_history_entry(op_dict: dict):
        """ Process account's history entry. """
        logger.debug(f"process history entry {op_dict}")
        operation, result = op_dict['op'], op_dict['result']
        if not operation:
            logger.warning('no operations found')
        op_code, op_info = operation[0], operation[1]
        op_class = Operation.get_op_cls_by_id(op_code)
        if op_class is not None:
            op = op_class(op_dict)
            await op.load()
            res = str(op)
        else:
            logger.warning(f'unknown op code {op_code}')
            res = None
        return res
        # raise UnknownOperationCodeError()
        # except RequiredFieldMissedError:
        #     logger.error('required field missed in {}'.format(history_entry))
        #     return None
        # except UnknownOperationCodeError:
        #     logger.error('unknown op_code: {} in {}'.format(
        #                  op_code, history_entry))
        #     return None

    async def broadcast_message(self, users: List['User'], msg: str):
        for user in users:
            if not user.status:
                continue
            # try:
            await self._queue.put({'id': user.id, 'msg': msg, 'parse_mode': 'Markdown'})
            # todo botblocked
            # except BotBlocked:
            #     logger.warning(f"User {user.tg_id} ('{user.username}') blocked the bot. Disable notifications.")
            #     user.status = 0
            #     session.commit()
            # dirty hack to avoid telegram limits. We can't send more frequently anyway.
            await asyncio.sleep(0.034)

    async def process_account(self, account: Account):
        logger.debug('process account {}'.format(account.name))
        update_msgs = []
        async for update_msg in self.account_updates_iter(account):
            if update_msg:
                update_msgs.append(update_msg)
        if update_msgs:
            msg = "New updates on account *{}*:\n\n • ".format(account.name)
            msg = msg + "\n • ".join(update_msgs)
            await self.broadcast_message(account.users, msg)

    @staticmethod
    async def disable_user(user_id: int):
        user = session.query(User).get(user_id)
        user.status = False
        session.commit()
        logger.info(f"disable telegram notifications on user {user}")

    async def run(self):
        while True:
            try:
                lookup_accounts = session.query(Account)\
                    .filter(Account.users.any(status=True))
                logger.debug('lookup_accounts: {}'.format(lookup_accounts))
                for lookup_account in lookup_accounts:
                    await self.process_account(lookup_account)
                session.expire_all()
                await asyncio.sleep(self.check_interval)
            except Exception as err:
                logger.error(err, exc_info=True)


async def run(check_interval: int = 15):
    logger.info('notifier service has been started')
    nw = await NotifierWorker(
        node=BITSHARES_NODE,
        check_interval=check_interval
    ).async_init()
    await nw.run()


if __name__ == '__main__':
    asyncio.run(run(CHECK_INTERVAL))
