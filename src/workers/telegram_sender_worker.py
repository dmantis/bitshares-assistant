import asyncio
import logging

from aiogram import Bot
from aiogram.utils.exceptions import BotBlocked


logger = logging.getLogger()


class TelegramSender(object):
    # todo check interval
    def __init__(self, bot_token: str, interval: float = 0.034):
        self._queue = asyncio.Queue()
        self._handlers = {
            'blocked': []
        }
        self._bot = Bot(token=bot_token)
        self._interval = interval

    @property
    def queue(self):
        return self._queue

    def on(self, handler_name, callback):
        if handler_name in self._handlers:
            self._handlers[handler_name].append(callback)
        else:
            logger.error(f"try to set undefined handler {handler_name}")

    async def run(self):
        while True:
            msg_data = await self._queue.get()
            logger.debug(f"send\n'{msg_data['msg']}'\nto chat {msg_data['id']}")
            try:
                await self._bot.send_message(
                    chat_id=msg_data['id'],
                    text=msg_data['msg'],
                    parse_mode=msg_data['parse_mode']
                )
            except BotBlocked:
                logger.warning(f"User {msg_data['id']} blocked the bot.")
                for handler in self._handlers['blocked']:
                    await handler(msg_data['id'])
            except Exception as err:
                logger.error(err, exc_info=True)
            await asyncio.sleep(self._interval)
